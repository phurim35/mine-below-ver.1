﻿using Photon.Pun;
using UnityEngine;
using ExitGames.Client.Photon;
using Photon.Realtime;
using UnityStandardAssets.Characters.FirstPerson;

public class PunHealth : MonoBehaviourPunCallbacks , IPunObservable 
{
    public const int maxHealth = 100;
    public int currentHealth = maxHealth;

    public void OnGUI()
    {
        if(photonView.IsMine)
            GUI.Label(new Rect(0, 0, 300, 50), "Player Health : " + currentHealth);
    }

    public void TakeDamage(int amount, int OwnerNetID)
    {
        if (photonView != null)
            photonView.RPC("PunRPCTakedDamage", RpcTarget.All, amount, OwnerNetID);
        else print("photonView is NULL.");
    }

    [PunRPC]
    public void PunRPCTakedDamage(int amount, int OwnerNetID)
    {
        Debug.Log("Take Damage");
        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            //Local Update Logic
            //if (!photonView.IsMine)
            //    return;
                
            Debug.Log("NetID : " + OwnerNetID.ToString() + " Killed " + photonView.ViewID);
            photonView.RPC("PunResetPlayer", photonView.Owner);
        }
    }

    public void Healing(int amout) {
        currentHealth += amout;

        if (currentHealth <= 0)
        {
            //Local Update Logic
            if (!photonView.IsMine)
                return;

            PunResetPlayer();
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        if (stream.IsWriting) {
            stream.SendNext(currentHealth);
        }
        else {
            currentHealth = (int) stream.ReceiveNext();
        }
    }

    [PunRPC]
    public void PunResetPlayer()
    {
        Debug.Log("Reset Position..");
        Vector3 Direction = AirDrop.RandomPosition(20) - this.transform.position;
        this.GetComponent<CharacterController>().Move(Direction);
        currentHealth = maxHealth;
    }
}
