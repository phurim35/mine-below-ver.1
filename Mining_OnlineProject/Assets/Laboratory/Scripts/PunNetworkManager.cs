﻿using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using ExitGames.Client.Photon;

//Fix setting for Game Network State Management
using UnityEngine.SceneManagement;
using System.Collections;
using Photon.Pun.Demo.PunBasics;

public class PunNetworkManager : ConnectAndJoinRandom, IOnEventCallback
{
    public static PunNetworkManager singleton;

    [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public GameObject GamePlayerPrefab;

    public bool isGameStart = false;
    public bool isFirstSetting = false;
    public bool isGameOver = false;
    //Fix setting for Game Network State Management
 //   public bool isLoadLevel = false;

    //public GameObject HealingPrefab;
    public int numberOfHealing = 5;
    float m_count = 0;
    public float m_CountDownDropHeal = 10;
    /// <summary>
    /// Create delegate Method
    /// </summary>
    public delegate void PlayerSpawned();
    public static event PlayerSpawned OnPlayerSpawned;

    public delegate void FirstSetting();
    public static event FirstSetting OnFirstSetting;

  //  public GameObject RocketPrefab;

    

    //Fix setting for Game Network State Management
     public Camera SceneCamera;

    [Tooltip("The UI Loader Anime")]
    [SerializeField]
    private LoaderAnime loaderAnime;
    
    private void Awake()
    {
        //Fix setting for Game Network State Management
        //Check if existing instance of class exists in scene
        //If so, then destroy this instance
        if (singleton)
        {
            DestroyImmediate(gameObject);
            return;
        }
        //Make this active and only instance
        singleton = this;

        //Add Reference Method to Delegate Method
        OnPlayerSpawned += SpawnPlayer;
        OnFirstSetting += FirstSettingAirDropHealing;

        //Fix setting for Game Network State Management
        //When Connected from Lancher Scene
        if (PhotonNetwork.IsConnected)
        {
            if (SceneCamera != null)
                SceneCamera.gameObject.SetActive(false);

            if (PunUserNetControl.LocalPlayerInstance == null)
                OnPlayerSpawned();
            if (isGameStart == false)
            {
                isGameStart = true;
                OnFirstSetting();
            }

        }
    }

    private void Update()
    {
        //if(isLoadLevel && loaderAnime != null)
        //    loaderAnime.StartLoaderAnimation();

        if (PhotonNetwork.IsMasterClient != true)
            return;

        if (isGameStart == true)
        {
            if (isFirstSetting == false)
                OnFirstSetting();

            if (isFirstSetting == true)
                AirDropHealing();
            else AirDropHealing();

            // Raise Event create healing with host only.
            if (Input.GetKeyDown(KeyCode.H))
            {
                CallRaiseEvent();
            }
        }

        //Fix setting for Game Network State Management
        //Try Load Network Scene
        //if (Input.GetKeyDown(KeyCode.N) && isLoadLevel == false)
        //    StartCoroutine("LoadArena");
    }

    private void FirstSettingAirDropHealing()
    {
        isFirstSetting = true;

        //int half = numberOfHealing / 2;
        //for (int i = 0; i < half; i++)
        //{
        //    PhotonNetwork.InstantiateRoomObject(HealingPrefab.name
        //                        , AirDrop.RandomPosition(5f)
        //                        , AirDrop.RandomRotation()
        //                        , 0);
        //}
        //m_count = m_CountDownDropHeal;
    }

    private void AirDropHealing()
    {
        //if (GameObject.FindGameObjectsWithTag("Healing").Length < numberOfHealing)
        //{
        //    m_count -= Time.deltaTime;

        //    if (m_count <= 0)
        //    {
        //        m_count = m_CountDownDropHeal;
        //        PhotonNetwork.InstantiateRoomObject(HealingPrefab.name
        //            , AirDrop.RandomPosition(10f)
        //            , AirDrop.RandomRotation()
        //            , 0);
        //    }
        //}
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);

        Debug.Log("OnPlayerLeftRoom() " + newPlayer.NickName); // seen when other disconnects

    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        if (SceneCamera != null)
            SceneCamera.gameObject.SetActive(false);

        Debug.Log("New Client Call.");
        //Delegate Function Call when Player Joined to Room.
        OnPlayerSpawned();
    }

    public void SpawnPlayer()
    {
        if (PunUserNetControl.LocalPlayerInstance == null)
        {
            Debug.Log("We are Instantiating LocalPlayer from " + SceneManagerHelper.ActiveSceneName);
            //PunNetworkManager.singleton.SpawnPlayer();
            // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
            PhotonNetwork.Instantiate(GamePlayerPrefab.name,
                new Vector3(0f,20f, 0f), Quaternion.identity, 0);

            isGameStart = true;
        }
        else
        {
            Debug.Log("Ignoring scene load for " + SceneManagerHelper.ActiveSceneName);
        }

    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);

        //Fix setting for Game Network State Management
        if (SceneCamera != null)
            SceneCamera.gameObject.SetActive(true);
    }

    #region Room Properties
    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
    {
        base.OnRoomPropertiesUpdate(propertiesThatChanged);
        Debug.Log(propertiesThatChanged.Values.Count);
        object GameOver;
        if (propertiesThatChanged.TryGetValue(PunGameSetting.GAMEOVER, out GameOver))
        {
            Debug.Log("Game Over is : " + GameOver);
            isGameOver = (bool)GameOver;
        }
    }
    #endregion

    #region Propoties Event and Raise Event Registor

    // Raise Event
    // Custom Event 10: Used as "RandomCallAirDropEvent" event
    private readonly byte RandomCallAirDropEvent = 10;

    public override void OnEnable()
    {
        base.OnEnable();
        // Raise Event
        PhotonNetwork.AddCallbackTarget(this);

        // LoadBalancingClient.EventReceived
        // The second way to receive custom events is to register a method
        //PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        // Raise Event
        PhotonNetwork.RemoveCallbackTarget(this);

        // LoadBalancingClient.EventReceived
        // The second way to receive custom events is to register a method
        //PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }
    #endregion

    #region Raise Event
    // RaiseEvent with Local GameObject.
    private void CallRaiseEvent()
    {
        //// Array contains the target position and the IDs of the selected units
        //object[] content = new object[] { AirDrop.RandomPosition(80f), Random.Range(0, 7) };

        //// You would have to set the Receivers to All in order to receive this event on the local client as well
        //RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };

        //SendOptions sendOptions = new SendOptions { Reliability = true, Encrypt = true };

        //PhotonNetwork.RaiseEvent(RandomCallAirDropEvent, content, raiseEventOptions, sendOptions);
        //Debug.Log("Call Raise Event.");
    }

    public void OnEvent(EventData photonEvent)
    {
        //Debug.Log(photonEvent.ToStringFull());

        byte eventCode = photonEvent.Code;

        if (eventCode == RandomCallAirDropEvent)
        {

            //Debug.Log("Call Resise Event is : " + eventCode.ToString());
            //object[] data = (object[])photonEvent.CustomData;

            //Vector3 position = (Vector3)data[0];
            //int color = (int)data[1];

            //Debug.Log("Position : " + position);
            //Debug.Log("Color : " + color);

            // Instance Local Object
           // GameObject localRocket = Instantiate(RocketPrefab);
            //Color currentColor = PunGameSetting.GetColor(color);
            //localRocket.transform.position = position;
            //localRocket.GetComponent<RocketBoom>().Damage *= color;
            //localRocket.GetComponent<MeshRenderer>().material.color = currentColor;
        }

    }
    #endregion

    #region Game Network State Management

    /// <summary>
    /// Called when the local player left the room. We need to load the launcher scene.
    /// </summary>
    public override void OnLeftRoom()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        SceneManager.LoadScene("PunBasics-Launcher");
    }

    #region Public Methods

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    #endregion

    #region Private Methods

    private IEnumerator LoadArena()
    {
        isGameStart = false;
       // isLoadLevel = true;
        PhotonNetwork.IsMessageQueueRunning = false;
        GameObject[] healingObj = GameObject.FindGameObjectsWithTag("Healing");

        for (int i = 0; i < healingObj.Length; i++)
            Destroy(healingObj[i]);

        Debug.Log("Start wait Remove Network Object " + Time.time);
        Debug.Log("For 3 Seconds");
        yield return new WaitForSeconds(3f);
        Debug.Log("End wait Remove Network Object " + Time.time);

        if (!PhotonNetwork.IsMasterClient)
        {
            Debug.LogError("PhotonNetwork : Trying to Load a level but we are not the master Client");
        }

        Debug.LogFormat("PhotonNetwork : Loading Level : {0}", PhotonNetwork.CurrentRoom.PlayerCount);
        PhotonNetwork.IsMessageQueueRunning = true;
        PhotonNetwork.LoadLevel("PunBasics-Room for " + PhotonNetwork.CurrentRoom.PlayerCount);
    }

    #endregion

    #endregion
}
