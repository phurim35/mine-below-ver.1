﻿using UnityEngine;
public class RocketBoom : MonoBehaviour
{
    public int Damage = 5;
    public LayerMask _playerMask;
    private void OnCollisionEnter(Collision other) {
        Collider[] _player = Physics.OverlapSphere(this.transform.position, 10, _playerMask);
        for (int i = 0; i < _player.Length; i++) {
            PunHealth otherHeal = _player[i].gameObject.GetComponent<PunHealth>();
            otherHeal.Healing(Damage * -1);
        }

        Destroy(this.gameObject);
    }

}
