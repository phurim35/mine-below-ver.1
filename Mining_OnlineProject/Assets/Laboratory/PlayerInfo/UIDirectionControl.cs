﻿using UnityEngine;

public class UIDirectionControl : MonoBehaviour {
    
    public Transform LocalDirection;
    Camera currentCam = null;
    // Use this for initialization
    void Start () {

        GameObject[] list = GameObject.FindGameObjectsWithTag("Player");
        Debug.Log(list.Length);
        for (int i = 0; i < list.Length; i++)
        {
            
            
                currentCam = list[i].GetComponentInChildren<Camera>();
            
        }

        if (currentCam != null)
            LocalDirection = currentCam.transform;

        RectTransform[] textlist = GetComponentsInChildren<RectTransform>();
        for(int i = 0; i < textlist.Length; i++) {
            textlist[i].Rotate(Vector3.up, 180);
        }
    }
	
	// Update is called once per frame
	void Update () {
        this.transform.LookAt(LocalDirection);
    }
}
