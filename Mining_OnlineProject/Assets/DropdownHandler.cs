﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Photon.Realtime;


public class DropdownHandler : MonoBehaviourPunCallbacks
{
    #region Singleton
    public static DropdownHandler singleton;
    private void Awake()
    {
        if (singleton != null)
        {
            return;
        }
        singleton = this;
       
    }
    #endregion

    public Dropdown dropdown;
    
    
    
    void Start()
    {
        
        //Debug.Log("Select: " + dropdown.value);
        dropdown.onValueChanged.AddListener(delegate
        {

            DropdownChange(dropdown);
            
        });
    }
    private void Update()
    {
        
    }
    public void DropdownChange(Dropdown dropdown)
    {
        Hashtable props = new Hashtable
        {
            {PunGameSetting.MAP_STATE,dropdown.value }
        };
        
        PhotonNetwork.LocalPlayer.SetCustomProperties(props);
        
    }
    
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        object State;

        if (changedProps.TryGetValue(PunGameSetting.MAP_STATE, out State))
        {
            dropdown.value = (int)State;
            Debug.Log("Select: " + dropdown.value);
        }
    }


}
