﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Digging : MonoBehaviour
{
    

    public Camera myEye;

    public float clickStart ;
    public float clickTime;

    public bool canDestroy;
    public bool isRemoving=false;

    private Box TargetBox;
    public Mesh meshcube;
    public TypeOfBox TodirtBox;
    public TypeOfBox MineTrap;
    public GrendeThrower mygrende;
    

    public Slider RemoveProgressBar;
    public GameObject RemoveBar;

    public bool isAttacking=false;

    public int currentHandle=0;


    public InventoryManager Inventory;


   


    void Update()
    {
        #region SelectedItem
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            currentHandle = 0;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            currentHandle = 1;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            currentHandle = 2;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            currentHandle = 3;
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            currentHandle = 4;
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            currentHandle = 5;
        }

        #endregion

        if (Input.GetMouseButton(0))
        {
            
            clickStart -= Time.deltaTime;
            if (currentHandle == 0 && RemoveProgressBar != null && RemoveBar != null)
            {
                Ray ray = myEye.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0f));
                RaycastHit hit;

                
                if (Physics.Raycast(ray, out hit, 3f))
                {
                    
                    if (hit.collider.gameObject.CompareTag("Box"))
                    {
                        RemoveBar.SetActive(true);
                        RemoveProgressBar.value = clickStart;
                    }
                    else
                        RemoveBar.SetActive(false);

                }
                
            }
            

        }


        if (Input.GetMouseButtonDown(0))
            clickTime = Time.time;
        if (Input.GetMouseButtonUp(0) && Time.time - clickTime >= 1)
        {
            
            isRemoving = true;
            clickStart = 1;
        }
        if (Input.GetMouseButtonUp(0) && Time.time - clickTime < 1)
        {
            
            isRemoving = false;
            clickStart = 1;

            if (currentHandle == 0)
            {
                Attack();
            }
            if (currentHandle >0)
                UseItem1(currentHandle);






        }
        if (Input.GetMouseButtonUp(0))
        {
            if (currentHandle == 2 && RemoveProgressBar != null && RemoveBar != null)
                RemoveBar.SetActive(false);
        }

                                                                                            //Add Cube
        if (Input.GetMouseButtonDown(1))
            clickTime = Time.time;
        if (Input.GetMouseButtonUp(1) && Time.time - clickTime >= 1)
        {

            isRemoving = true;
            clickStart = 1;
        }
        if (Input.GetMouseButtonUp(1) && Time.time - clickTime < 1)
        {
            if (Inventory.BoxType[0].total > 0&&currentHandle==0)
            {
                AddCube();
            }
                
            isRemoving = false;
            clickStart = 1;
        }


                                                                                        //Remove Cube
        if (canDestroy == false)
        {
            if (clickStart <= 0)
            {

                RemoveCube();
                canDestroy = true;
                clickStart = 1;
                

            }
        }
        if (clickStart == 1)
        {
            canDestroy = false;
        }

        if (currentHandle == 0)
        {
            isAttacking = true;
        }
        else
        {
            isAttacking = false;
        }

        HandleState();


    }
    void UseItem1(int indexofSection)
    {
        if(Inventory.ItemType[indexofSection-1].total <= 0)
        {
            return;
        }
        if(indexofSection == 1)
        if (Inventory.ItemType[indexofSection-1].TypeOf == TypeOfItem.Type.Grenade )
        {
                mygrende.ThrowGrenade();                             //Trow Granade
        }
        if (indexofSection == 2)
            if (Inventory.ItemType[indexofSection-1].TypeOf == TypeOfItem.Type.Minetrap)
            {
                canvertItemToBox(Inventory.ItemType[indexofSection - 1], MineTrap);
             AddCube();
           
                                        //Trow Granade
            }


        Inventory.ItemType[indexofSection - 1].total -= 1;
    }
    void AddCube()
    {
      
            
            Ray ray = myEye.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0f));
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 3f))
            {
                
                TargetBox = hit.collider.gameObject.GetComponent<Box>();
                if (hit.collider.gameObject.CompareTag("Box") /* && TargetBox.boxsetting.TypeOf == TypeOfBox.Type.Ground */)
                {
                    
                    //Add cube.
                    
                    Vector3 newPos = hit.transform.position;
                    GameObject newBox = Instantiate(hit.collider.gameObject);
                    newBox.gameObject.isStatic = false;
                    TargetBox = newBox.GetComponent<Box>();
                 
                    if (currentHandle==2)
                    {
                        TargetBox.boxsetting = MineTrap;
                    }
                    else TargetBox.boxsetting = TodirtBox;


                    newBox.name = hit.collider.gameObject.name + "newcube";
                    MeshFilter mf = newBox.GetComponent<MeshFilter>();
                    mf.mesh = meshcube;
                    newBox.transform.position = newPos;

                    
                    

                    newBox.transform.Translate(hit.normal * 1f);
                    newBox.gameObject.tag = "Box";
                    newBox.gameObject.isStatic = true;


                    TargetBox.DecreaseStackBox();
                }

            }
        
    }
    void RemoveCube()
    {
        
        if (currentHandle == 0)
        {
        Ray ray = myEye.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0f));
        RaycastHit hit;

        isRemoving = true;

            if (Physics.Raycast(ray, out hit, 3f))
            {

            //Remove cube.
                if (hit.collider.gameObject.CompareTag("Box"))
                {
                    TargetBox = hit.collider.gameObject.GetComponent<Box>();
                    TargetBox.SpawnItem();
                    TargetBox.Exploed();
                    TargetBox.StackBox();   
                    
                    Destroy(hit.collider.gameObject);

                    
                       


                }
                

            }

        }


    }
    void Attack()
    {
        if (currentHandle == 0)
        {
            Ray ray = myEye.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0f));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 2f))
            {
                if (hit.collider.gameObject.CompareTag("Player"))
                {
                    Destroy(hit.collider.gameObject);
                }
            }
        }
    }

    void HandleState()
    {
        if (currentHandle > 5)
            currentHandle = 0;
        if (currentHandle < 0)
            currentHandle = 5;
        
        currentHandle += (int)Input.mouseScrollDelta.normalized.y;
    }
    

    void canvertItemToBox(TypeOfItem item,TypeOfBox box)
    {
        box.Force = item.Force;
        box.Mat = item.Mat;
        box.radius = item.radius;
        box.explosionEffect = item.explosionEffect;
        if(item.TypeOf == TypeOfItem.Type.Minetrap&&box.TypeOf!=TypeOfBox.Type.TNTbox)
        {
            box.TypeOf = TypeOfBox.Type.TNTbox;
        }

    }
}
