﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[System.Serializable]
public class Box : MonoBehaviourPun
{
    public TypeOfBox boxsetting;
    private int r;
    private bool Randomed = false;
    public InventoryManager[] Inventory=null;
    public PlayerHealth HP;
    public SerializedData serializedData;

    public int numOfBox=0;
    // Start is called before the first frame update
    void Start()
    {
        //GameObject[] list = GameObject.FindGameObjectsWithTag("Player");
        //for (int i = 0; i < list.Length; i++)
        //{
        //    if (list[i].GetComponentInChildren<InventoryManager>().enabled)
        //    {
        //        Inventory[i] = list[i].GetComponentInChildren<InventoryManager>();
        //    }
        //}
        HP =GameObject.FindObjectOfType<PlayerHealth>();

        if (boxsetting.TypeOf == TypeOfBox.Type.RandomBox)
        {
            
            RandomType();
        }
            
        RandomItem();




       // Inventory = GameObject.FindObjectOfType<InventoryManager>();

        if (boxsetting.TypeOf == TypeOfBox.Type.Ground)
        {
            numOfBox = 1;
            
        }
        if (boxsetting.TypeOf == TypeOfBox.Type.TNTbox)
        {
            numOfBox = 2;
        }
        if (boxsetting.TypeOf == TypeOfBox.Type.DropBox)
        {
            numOfBox = 0;
        }

        
            //serializedData = new SerializedData(numOfBox);


    }
    void Update()
    {
        var cubeRenderer = gameObject.GetComponent<Renderer>();
        cubeRenderer.material = boxsetting.Mat;

        if (this.numOfBox == 0)
        {

            boxsetting = boxsetting.RandomType[0];
            
        }
        if (this.numOfBox == 1)
        {
            
            boxsetting = boxsetting.RandomType[1];
            
        }
        if (this.numOfBox == 2)
        {
            
            boxsetting = boxsetting.RandomType[2];
            
        }


    }

    // Update is called once per frame


    void RandomItem()
    {
        if (boxsetting.TypeOf == TypeOfBox.Type.DropBox)
        { if (!Randomed)
                r = Random.Range(0, boxsetting.itemDropPool.Count);

        }
    }

    public void SpawnItem()
    {
        if (boxsetting.TypeOf == TypeOfBox.Type.DropBox)
        {
            GameObject NewItem = Instantiate(boxsetting.itemDropPool[r]);
            NewItem.transform.position = transform.position;

        }

    }

    void RandomType()
    { r = Random.Range(0, 100);
        if (r <= 5)
            r = 2;
        else if (r > 5 && r <= 25)
            r = 0;
        else if(r>25)
            r = 1;
        
        boxsetting = boxsetting.RandomType[r];
    }
    
  public void Exploed()
    {
        if (boxsetting.TypeOf == TypeOfBox.Type.TNTbox)
        {
            if (boxsetting.explosionEffect != null)
                Instantiate(boxsetting.explosionEffect, transform.position, transform.rotation);
            Debug.Log("Boooom");
            // get nearby object
            Collider[] colliderToDestory = Physics.OverlapSphere(transform.position, boxsetting.radius);

            foreach (Collider nearbyObject in colliderToDestory)
            {
                if (nearbyObject.CompareTag("Box"))
                {
                    Destroy(nearbyObject.gameObject);
                }

            }
            Collider[] colliderToMove = Physics.OverlapSphere(transform.position, boxsetting.radius);
            foreach (Collider nearbyObject in colliderToMove)
            {
                Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.AddExplosionForce(boxsetting.Force, transform.position, boxsetting.radius);
                  //  HP.TakeDamage(10);
                }
            }
        }
    } 
    public void StackBox()
    {
        //Add Stack of Box
        if (boxsetting.TypeOf == TypeOfBox.Type.Ground)
        {
            Debug.Log("Box");
           // Inventory.BoxType[0].total += 1;

        }
    }
    public void DecreaseStackBox()
    {
        if (boxsetting.TypeOf == TypeOfBox.Type.Ground)
        {
            Debug.Log("DeBox");
       //     Inventory.BoxType[0].total -= 1;

        }
    }

    //[PunRPC]
    //private void PunRPCDestroy()
    //{
    //    Destroy(this.gameObject);
    //}

    //private void OnDestroy()
    //{
    //    if (!photonView.IsMine)
    //        return;

    //    photonView.RPC("PunRPCDestroy", RpcTarget.All);
    //}
    //public void Array3D(int type)
    //{
        
    //    //serializedData = new SerializedData(type);
    //}
}
