﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrendeThrower : MonoBehaviour
{
    public float throwForce = 40f;
    public TypeOfItem prefabs;
    public InventoryManager Inventory;
    public Digging Dig;

  public  void ThrowGrenade()
    {
      GameObject grenade=  Instantiate(prefabs.model, transform.position+(transform.forward*2), transform.rotation);
        Rigidbody rb = grenade.GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * throwForce,ForceMode.VelocityChange);

        //  Inventory.Item.Remove(prefabs);           //Remove Item from Inventory
       
        Inventory.SeclectedItem[Dig.currentHandle].SetActive(false);
       // Inventory.inventory[Dig.currentHandle-1].sprite = null;


    }
}
