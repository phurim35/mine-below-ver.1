﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="TypeOfBoxsetting",menuName ="Box",order =1)]
public class TypeOfBox : ScriptableObject
{

    public enum Type{DropBox,Ground,TNTbox,RandomBox,Grenade};
    public Type TypeOf;
    public TypeOfBox[] RandomType;
    public List<GameObject> itemDropPool;
    public GameObject explosionEffect;
    public float radius = 5f;
    public float Force = 700;
    public Material Mat;
    public Sprite sprt;
    public int total;

}
