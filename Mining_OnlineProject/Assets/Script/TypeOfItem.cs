﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TypeOfItemSetting", menuName = "ItemSetting", order = 2)]
public class TypeOfItem :ScriptableObject
{
    public enum Type { Grenade,Minetrap, Ammor };
    public Type TypeOf;
    public GameObject explosionEffect;
    public float radius = 5f;
    public float Force = 700;
    public GameObject model;
    public Material Mat;
    public Sprite sprt;
    public int total;


}


