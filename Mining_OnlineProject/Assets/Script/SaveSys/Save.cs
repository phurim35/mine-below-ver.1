﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
[System.Serializable]
public  static class Save 
{
  public static void SaveBox(Box box)
    {
        BoxData data = new BoxData(box);
        string path = Application.persistentDataPath + "/Box.text";
        FileStream stream = new FileStream(path, FileMode.Create);

        BinaryFormatter formatter = new BinaryFormatter();
        formatter.Serialize(stream, data);
  
        stream.Close();
    }
    public static BoxData LoadBox()
    {
        string path=Application.persistentDataPath + "/Box.text";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

           BoxData  data= formatter.Deserialize(stream) as BoxData;
            stream.Close();
            return data;

        }
        else
        {
            Debug.Log("Save file not found in" + path);
            return null;
        }

    }
}
