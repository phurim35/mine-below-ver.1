﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SavethisBox : MonoBehaviour
{
    public Box BOXXXXXX;
    // Start is called before the first frame update
   public void SaveBox()
    {
        Save.SaveBox(BOXXXXXX);
    }
    public void LoadBox()
    {
        BoxData data = Save.LoadBox();
        BOXXXXXX = data._box;
    }

    public void Update()
    {
         if(Input.GetKeyDown(KeyCode.K))
        {
            SaveBox();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadBox();
        }
    }
}
