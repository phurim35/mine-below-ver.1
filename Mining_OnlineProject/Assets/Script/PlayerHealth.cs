﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
	public float maxHealth = 10;
	public float currentHealth;
	public int Ammor = 0;
	public HealthBar healthBar;

	// Start is called before the first frame update
	void Start()
	{
		currentHealth = maxHealth;
		healthBar.setMaxHealth(maxHealth);
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Q))
		{
			TakeDamage(1);
		}
	}

public	void TakeDamage(float damage)
	{
        if (Ammor > 0)
        {
			Ammor--;
			damage /=2;
        }
		currentHealth -= damage;

		healthBar.setHealth(currentHealth);
	}
}
