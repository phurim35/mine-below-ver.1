﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenada : MonoBehaviour
{
    public float delay = 3f;
    float countdown;
    bool hasExploded = false;

    public TypeOfItem setting;
    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;
        if(countdown<=0&&!hasExploded||Input.GetMouseButtonDown(1))
        {
            Explode();
            hasExploded = true;
        }
        
    }

    void Explode()
    {//show effect
        if(setting.explosionEffect != null)
        Instantiate(setting.explosionEffect, transform.position, transform.rotation);
        Debug.Log("Boooom");
        // get nearby object
    Collider[] colliderToDestory =    Physics.OverlapSphere(transform.position, setting.radius);
        
        foreach(Collider nearbyObject in colliderToDestory)
        {
            if (nearbyObject.CompareTag("Box"))
            {
                Destroy(nearbyObject.gameObject);
            }
      
        }
        Collider[] colliderToMove = Physics.OverlapSphere(transform.position, setting.radius);
        foreach (Collider nearbyObject in colliderToMove)
        {
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(setting.Force, transform.position, setting.radius);
            }
        }
        //addForce
        //Damage
        Destroy(gameObject);
    }
}
