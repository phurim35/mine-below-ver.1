﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpItem : MonoBehaviour
{
    
    public TypeOfItem ItemType;
    public InventoryManager inventory;
    public PlayerHealth Hp;
    public void Start()
    {
        inventory = GameObject.FindObjectOfType<InventoryManager>();
        Hp= GameObject.FindObjectOfType<PlayerHealth>();
    }
    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player")&&inventory.ItemType.Length<=6)
        {
            AddItem();
            
            Destroy(gameObject);
        }
    }
    public void AddItem()
    {
        // inventory.Item.Add(ItemType);
        if (this.ItemType.TypeOf == TypeOfItem.Type.Grenade)
            inventory.ItemType[0].total += 1;
        if (this.ItemType.TypeOf == TypeOfItem.Type.Minetrap)
            inventory.ItemType[1].total += 1;
        if(this.ItemType.TypeOf== TypeOfItem.Type.Ammor)
        {
            Hp.Ammor++;
        }

    }
    


}
