﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InventoryManager : MonoBehaviour
{
 

    public TypeOfItem[] ItemType;

    public Image[] inventory;
    public GameObject[] SeclectedItem;
    public Sprite _sprite;
    public Digging Slot;
    public Text[] StackofItem;

    public TypeOfBox[] BoxType;
    public Text[] StackofBox;
    public Image[] boxImage;

    

    // Start is called before the first frame update
    void Start()
    {
        foreach(GameObject all in SeclectedItem)
        {
            all.SetActive(false);
        }

        SeclectedItem[0].SetActive(true);

        foreach (TypeOfItem allItem in ItemType)
        {
            ItemType[0].total = 0;
        }
        foreach (TypeOfBox allItem in BoxType)
        {
            BoxType[0].total = 0;
        }




    }

    // Update is called once per frame
    void Update()
    {

        for (int i = 0; i < ItemType.Length; i++)
        {
            if (ItemType[i] != null)
            {
                inventory[i].sprite = ItemType[i].sprt;

                StackofItem[i].text = ItemType[i].total.ToString();
            }
        
   
   
   
        
        }
        for (int i = 0; i < BoxType.Length; i++)
        {
            if (BoxType[i] != null)
            {
                boxImage[i].sprite = BoxType[i].sprt;

                StackofBox[i].text = BoxType[i].total.ToString();
            }
        }


        //if(Item.Count==0)
        //   {
        //       for (int i = 0; i < Item.Count; i++)
        //       {

        //           if (Item[i] == null)
        //           {
        //               inventory[i].sprite = _sprite;
        //           }


        //       }
        //   }

        foreach (GameObject all in SeclectedItem)
        {
            all.SetActive(false);
            if (Slot.currentHandle >= 0 && Slot.currentHandle <= 5)
                SeclectedItem[Slot.currentHandle].SetActive(true);
          
           
           
        }

        

    }
    
    
}
