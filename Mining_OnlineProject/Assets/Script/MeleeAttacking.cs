﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttacking : MonoBehaviour
{
    

    Animator meleeAtt;
    
    void Start()
    {
        
        meleeAtt = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            
            meleeAtt.SetBool("Attacking", true);
        }
            
        if (Input.GetMouseButtonUp(0))
        {
            
            meleeAtt.SetBool("Attacking", false);
        }
            
    }
}
